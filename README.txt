####################################################################
###                 Freezone Port for XAseco                     ###
####################################################################
###  !!! I am not responsible if your server gets disabled !!!   ###
####################################################################

##Installation##

1. UnZIP the Archive into the XAseco folder.
2. Open the file "freezone.xml" and set <password> to your webservices-password.
3. Insert the following line into "plugins.xml":

    <plugin>plugin.freezone.php</plugin>

4. Restart XAseco

##Copyright##
The full copyright remains to the Author of the Freezone plugin for ManiaLive.
